import './App.css';
import Friends from './friends';

function App() {
  return (
    <div className="App" data-testid="friends">
      <Friends  />
    </div>
  );
}

export default App;
