            
import React from 'react'
import '../friends/friends.css'
const Pagination = (props) => {
    const {totalPages,pageIndex} = props
    return (
        <div className="pagination">
            <i className={"fa fa-arrow-left mrg-r-10 "+ (pageIndex >0 ? 'active': '')} onClick={()=>props.setPageNumber('prev')} />
             <i className={"fa fa-arrow-right "+ (totalPages > pageIndex + 1 ? 'active': '')} onClick={()=>props.setPageNumber('next')} />
        </div>
    )
}
export default Pagination