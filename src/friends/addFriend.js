import React from 'react'
import './friends.css'
const AddFriend = (props) => {
    
    const [friendName, setFriendName] = React.useState("");

    const handleKeyPress=(e)=>{
        const regex = /^[a-zA-Z ]{2,30}$/;

        if(e.keyCode == 13){
            if(friendName.trim().length==0){
                alert("Enter a name first")
                return
            }
            if(!regex.test(friendName)){
                alert("Enter a valid name")
                return
            }
            props.addFriend(friendName.trim());
            setFriendName("")
        }
    }

    
    return (
        <div >
            <input type="text" value={friendName} onChange={e=>setFriendName(e.target.value)} onKeyDown={handleKeyPress} placeholder="Enter the name of new friend"/>
        </div>
    )
}
export default AddFriend