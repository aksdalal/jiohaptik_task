import React from 'react'
import './friends.css'

const List = (props) => {
    return (
        <div>
            { props.friendsList  && (
           <ul>
                { props.friendsList.map((friend,index)=> {
                   return ( <li key={index}>{friend.name}
                   <span>
                   {friend.isFav === true ? <i className="fa fa-star" onClick={()=>props.updateList(friend)}/> : <i className="fa fa-star-o" onClick={()=>props.updateList(friend)} />}
                    <i className="fa fa-trash" onClick={()=>props.updateList(friend,true)}></i>
                    </span>
                    </li> ) 
                })}
           </ul> )
            }
        </div>
    )
}
export default List