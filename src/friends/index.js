import React from 'react'
import './friends.css'
import AddFriend from './addFriend';
import FriendsList from './list';
import Pagination from '../pagination';
import ActionButtons from '../actionButtons';
import FriendSearch from '../friendSearch';

const list = [{ name: "Amit", isFav: false }, { name: "Kumar", isFav: false }, { name: "Singh", isFav: false },
{ name: "Ankit", isFav: false }, { name: "Chalie", isFav: true }, { name: "Chand", isFav: false }, { name: "Louis", isFav: true },
{ name: "Sam", isFav: false }, { name: "Dean", isFav: false }, { name: "Dodo", isFav: false }, { name: "Richard", isFav: true },
{ name: "Anu", isFav: false }, { name: "Anjali", isFav: false }, { name: "Sumit", isFav: false }, { name: "Sunny", isFav: true }
]

export default class Friends extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            friendsList: list,
            filteredList: [],
            searchText: '',
            inputType: 'enter',
            isPagination: false,
            pageIndex: 0,
            totalPages: 1,
            itemPerPage: 4,
            paginatedList: []

        }
    }
    addFriend = (friend) => {
        let {friendsList} = this.state;
        let tempFriend = { name: friend, isFav: false };
        let isFriendExist = friendsList.some(f => f.name.toLowerCase() == friend.toLowerCase())
        if (isFriendExist) {
            alert("Friend already exist with this name")
        }
        else {
            friendsList.unshift(tempFriend)
            this.setState({ friendsList,pageIndex:0 },()=>this.setPagination())
        }
    }
    updateList = (friend, remove) => {
        let {friendsList,filteredList} = this.state;
        let index = friendsList.findIndex(item => item==friend)
        if (remove) {
            if (window.confirm("Are you sure to delete") == true) {
                friendsList.splice(index, 1);
                if(filteredList.length>0){
                    let filterIndex = filteredList.findIndex(item => item==friend)
                    filteredList.splice(filterIndex, 1);
                    this.setState({filteredList})
                }
            } else {
                return
            }
        }
        else {
            friendsList[index].isFav ? friendsList[index].isFav = false : friendsList[index].isFav = true;
        }
        this.setState({ friendsList},()=>this.setPagination())
    }

    filterFriends = (value) => {
        let arr = this.state.friendsList
        if(value.trim().length==0){
            this.setState({ filteredList: [], searchText: value })
            return
        }
        arr = arr.filter(item => item.name.toLowerCase().indexOf(value.toLowerCase()) !== -1)
        if(arr.length>4){
           arr= arr.slice(0,4)
        }
        this.setState({ filteredList: arr, searchText: value })

    }
    sortFavFriends = () => {
        let arr = this.state.friendsList
        arr = arr.sort(f => f.isFav ? -1 : 1)
        this.setState({ friendsList: arr,pageIndex:0 },()=>this.setPagination())
    }
    setPagination = () => {
        let arr = this.state.friendsList;
        let totalPages = Math.ceil((arr.length) / this.state.itemPerPage)

        var result = arr.reduce((resultArray, item, index) => {
            const chunkIndex = Math.floor(index / this.state.itemPerPage)

            if (!resultArray[chunkIndex]) {
                resultArray[chunkIndex] = []
            }
            resultArray[chunkIndex].push(item)

            return resultArray
        }, [])
        this.setState({ paginatedList: result, totalPages })
    }
    setPageNumber = (page) => {
        let { totalPages, pageIndex } = this.state
        if (page == 'next' && (pageIndex + 1) < totalPages) {
            this.setState({ pageIndex: pageIndex + 1 })
        }
        else if(page == 'prev' && (pageIndex - 1) >= 0){
            this.setState({ pageIndex: pageIndex - 1 })
        }
    }
    handleInputType = (inputType) =>{
       this.setState({inputType})
    }
    componentDidMount() {
        if (this.state.friendsList.length > 4) {
            this.setState({ isPagination: true })
            this.setPagination()
        }
    }
    render() {
        const { filteredList, inputType, searchText, isPagination, paginatedList, pageIndex, totalPages } = this.state;
        return (
            <div className="friendsLayout" data-testid="friendlayout">
                <div className="friendsCard">
                    <div className="friendsHeader"><p><b>Friends List</b></p></div>
                    <ActionButtons inputType={inputType} handleInputType={this.handleInputType} sortFavFriends={this.sortFavFriends} />
                    {inputType == "search" && <FriendSearch value={searchText} data={filteredList} handleFriendSearch={this.filterFriends} />}
                    {inputType == "enter" && <AddFriend addFriend={this.addFriend} />}
                    <FriendsList friendsList={inputType == "enter" ? paginatedList[pageIndex] : filteredList} updateList={this.updateList} />
                    {isPagination && inputType == "enter" && <Pagination setPageNumber={this.setPageNumber} pageIndex = {pageIndex} totalPages = {totalPages} />}
                </div>
            </div>
        )
    }
}
