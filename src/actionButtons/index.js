import React from 'react'
const ActionButtons = (props) => {
    
    return (
        <div className="actionButton">
        <i className={'fa fa-user-plus ' + (props.inputType == "enter" ? 'active' : '')} onClick={() => props.handleInputType('enter')} />
        <i className={'fa fa-search ' + (props.inputType == "search" ? 'active' : '')} onClick={() => props.handleInputType('search')} />
        {props.inputType == "enter" && <i className="fa fa-star" onClick={() => props.sortFavFriends()} />}
    </div>
    )
}
export default ActionButtons