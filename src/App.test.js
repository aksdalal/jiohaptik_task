import { render, screen } from '@testing-library/react';
import App from './App';
import Friends from './friends'
import List from './friends/list'
import Pagination from './pagination'

let mockList = [{ name: "Amit", isFav: false }, { name: "Kumar", isFav: false }, { name: "Singh", isFav: false }]
test('renders the friends app', () => {
  render(<App />);
  const linkElement = screen.getAllByTestId('friends');
  expect(linkElement.length).toBe(1);
});

describe('Friends component',()=>{
it('should render the friends component',()=>{
  render(<Friends />);
  const element = screen.getAllByTestId('friendlayout');
  expect(element.length).toBe(1);

  const contentElement = screen.getByText('Friends List');
  expect(contentElement).toBeInTheDocument()

})

})
describe('List component',()=>{
   const mockFn = jest.fn();
 it("should render the list with three items",()=>{
  render(<List friendsList={mockList} />);
  const element = screen.getAllByText('Amit');
  expect(element.length).toBe(1);
  expect(screen.getAllByRole('listitem').length).toBe(3);
 }) 
 
 it("should render the list with four items",()=>{
  let newlist = [{name:"pushpa", isFav:true}]
  newlist = [...newlist,...mockList]
  render(<List friendsList={newlist} />);
 
  expect(screen.getByText('pushpa')).toBeInTheDocument();
  expect(screen.getAllByRole('listitem').length).toBe(4);
 }) 

 it("should call the updateList function",()=>{
  const {container} = render(<List friendsList={mockList} updateList={mockFn} />);
  let elem  = container.querySelector('.fa-trash')
  expect(elem).toBeInTheDocument()
  elem.click()
  expect(mockFn.mock.calls.length).toBe(1);
  expect(JSON.stringify(mockFn.mock.calls[0][0])).toContain('Amit');
 }) 

})

describe("Pagination",()=>{

 it('Next button should be active',()=>{
  const {container} = render(<Pagination totalPages={4} pageIndex={0} />);
  let elem = container.querySelector(".fa-arrow-right");
  expect(elem.classList).toContain('active')
  let elem1 = container.querySelector(".fa-arrow-left");
  expect(elem1.classList).not.toContain('active')
 })

 it('Prev button should be active',()=>{
  const {container} = render(<Pagination totalPages={4} pageIndex={3} />);
  let elem = container.querySelector(".fa-arrow-right");
  expect(elem.classList).not.toContain('active')
  let elem1 = container.querySelector(".fa-arrow-left");
  expect(elem1.classList).toContain('active')
 })

})


