import React from 'react'

export default class FriendSearch extends React.Component {

    render() {
        return (
            <div>
                <input
                    type="text" value={this.props.value}
                    onChange={e => this.props.handleFriendSearch(e.target.value)}
                    placeholder="Search your friend" />
            </div>
        );
    }
}
